@extends('layout.master')

@section('judul')
SELAMAT DATANG
@endsection

@section('content')

    <h1>{{$nama_depan}} {{$nama_belakang}}</h1>
   
    <h4>Terimakasih telah bergabung di Website Kami. Media Belajar kita bersama!</h4>

@endsection