<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg(){
        return view('halaman.form');
    }

    public function direct(Request $request){
        $nama_depan = $request['fname'];
        $nama_belakang = $request['lname'];
        return view('halaman.welcome', compact('nama_depan','nama_belakang'));
    }
}